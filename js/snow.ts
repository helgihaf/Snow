class Flake {
    size: number;
    x: number;
    y: number;
    tag: any;

    applyWind(seconds: number, windDirection: number) {
        this.x += seconds * windDirection;
    }

    applyGravity(seconds: number, fallSpeed: number) {
        this.y += 0.001 + fallSpeed * seconds * this.size;
    }
}

class Engine {
    flakesPerSecond: number;
    fallSpeed: number;
    windDirection: number;
    maxFlakes: number;
    flakes: Flake[];
    erased: Flake[];

    start() {
        this.flakes = []
    }

    update(elapsedSeconds: number) {
        this.moveExistingFlakes(elapsedSeconds);
        this.addNewFlakes(elapsedSeconds);
    }

    private moveExistingFlakes(seconds: number) {
        this.erased = [];
        let newFlakes: Flake[] = [];
        for (let i = 0; i < this.flakes.length; i++) {
            var flake = this.flakes[i];
            flake.applyWind(seconds, this.windDirection);
            flake.applyGravity(seconds, this.fallSpeed);
            if (flake.y > 1) {
                this.erased.push(flake);
            }
            else {
                newFlakes.push(flake);
            }
        }
        this.flakes = newFlakes;
    }

    private addNewFlakes(seconds: number) {
        if (this.flakes.length >= this.maxFlakes) {
            return;
        }
        let flakeCountTemp = this.flakesPerSecond * seconds;
        let flakeCount = Math.floor(flakeCountTemp);
        flakeCountTemp -= flakeCount;
        if (Math.random() < flakeCountTemp) {
            flakeCount++;
        }

        let flakeRows = Math.ceil(seconds);
        if (flakeRows == 0) {
            return;
        }

        let flakesPerRow = flakeCount / flakeRows;
        for (let row = 0, i = 0; row < flakeRows && i < flakeCount; row++) {
            for (let j = 0; j < flakesPerRow && i < flakeCount; j++ , i++) {
                let flake = new Flake();
                flake.x = Math.random();
                flake.y = -0.2 + j * this.fallSpeed;
                flake.size = Math.pow(Math.random(), 4) * 3;
                this.flakes.push(flake);
            }
        }
    }
}

class Point {
    x: number;
    y: number;
}

class Renderer {
    private lastRender: Date;
    private intervalHandle: number;
    private testDiv: HTMLDivElement;

    private localFeatures;

    sizeFactor: number = 8;
    sizeTerm: number = 12;

    constructor(private readonly engine: Engine, private readonly document: Document, private readonly canvas: HTMLElement, private readonly label: HTMLElement) {
        this.testDiv = document.createElement('div');

        this.localFeatures = {
            transform: {
                ie: this.has('-ms-transform'),
                moz: this.has('MozTransform'),
                opera: this.has('OTransform'),
                webkit: this.has('webkitTransform'),
                w3: this.has('transform'),
                prop: null // the normalized property value
            }
        };

        this.localFeatures.transform.prop = (
            this.localFeatures.transform.w3 ||
            this.localFeatures.transform.moz ||
            this.localFeatures.transform.webkit ||
            this.localFeatures.transform.ie ||
            this.localFeatures.transform.opera
        );
    }

    private has(prop) {
        // test for feature support
        var result = this.testDiv.style[prop];
        return (result !== undefined ? prop : null);
    }

    start() {
        this.engine.start();
        this.lastRender = new Date();
        this.intervalHandle = setInterval(() => this.renderFrame(), 5);
    }

    stop() {
        clearInterval(this.intervalHandle);
    }

    private renderFrame() {
        this.render();
        if (this.label != null) {
            this.label.textContent = this.engine.flakes.length.toString();
        }

        let now = new Date();
        let seconds = (now.getTime() - this.lastRender.getTime()) / 1000;
        this.lastRender = now;
        this.engine.update(seconds);
        for (let i = 0; i < this.engine.erased.length; i++) {
            let flake = this.engine.erased[i];
            let element: HTMLElement = flake.tag;
            element.parentElement.removeChild(element);
        }
    }

    private render() {
        for (var i = 0; i < this.engine.flakes.length; i++) {
            let flake = this.engine.flakes[i];
            let element: HTMLElement = flake.tag;
            if (element == null) {
                element = document.createElement("div");
                element.style.fontSize = (this.sizeTerm + flake.size * this.sizeFactor) + 'px';
                element.style.width = (this.sizeTerm + flake.size * this.sizeFactor) + 'px';
                element.style.height = (this.sizeTerm + flake.size * this.sizeFactor) + 'px';
                //element.style.backgroundColor = '#000';
                element.style.color = '#fff';
                element.style.fontFamily = 'arial,verdana';
                element.style.cursor = 'default';
                element.style.overflow = 'hidden';
                element.style.fontWeight = 'normal';
                element.style.position = 'absolute';
                element.innerHTML = '&bull;';
                element.style[this.localFeatures.transform.prop] = 'translate3d(0px, 0px, 0px)';

                this.canvas.appendChild(element);
                flake.tag = element;
            }

            let point: Point = this.convertFlakeLocation(flake);
            element.style.left = point.x + 'px';
            element.style.top = point.y + 'px';
        }
    }

    private convertFlakeLocation(flake: Flake): Point {
        //let windFactor: number = this.engine.windDirection * 16000;
        let point = new Point();
        let width: number = this.canvas.clientWidth - 50;
        let height: number = this.canvas.clientHeight - 50;
        point.x = flake.x * width; //- windFactor;
        point.y = flake.y * height;

        return point;
    }
}

var snowStorm = (function (window, document) {
    var engine = new Engine()
    engine.flakesPerSecond = 14;
    engine.fallSpeed = 0.03;
    engine.windDirection = 0.0;
    engine.maxFlakes = 150;

    var canvas = document.getElementById("canvas");
    var textMessageElement = document.getElementById("textMessage");

    let renderer = new Renderer(engine, document, canvas, textMessageElement);
    renderer.start()
}(window, document));