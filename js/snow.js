var Flake = /** @class */ (function () {
    function Flake() {
    }
    Flake.prototype.applyWind = function (seconds, windDirection) {
        this.x += seconds * windDirection;
    };
    Flake.prototype.applyGravity = function (seconds, fallSpeed) {
        this.y += 0.001 + fallSpeed * seconds * this.size;
    };
    return Flake;
}());
var Engine = /** @class */ (function () {
    function Engine() {
    }
    Engine.prototype.start = function () {
        this.flakes = [];
    };
    Engine.prototype.update = function (elapsedSeconds) {
        this.moveExistingFlakes(elapsedSeconds);
        this.addNewFlakes(elapsedSeconds);
    };
    Engine.prototype.moveExistingFlakes = function (seconds) {
        this.erased = [];
        var newFlakes = [];
        for (var i = 0; i < this.flakes.length; i++) {
            var flake = this.flakes[i];
            flake.applyWind(seconds, this.windDirection);
            flake.applyGravity(seconds, this.fallSpeed);
            if (flake.y > 1) {
                this.erased.push(flake);
            }
            else {
                newFlakes.push(flake);
            }
        }
        this.flakes = newFlakes;
    };
    Engine.prototype.addNewFlakes = function (seconds) {
        if (this.flakes.length >= this.maxFlakes) {
            return;
        }
        var flakeCountTemp = this.flakesPerSecond * seconds;
        var flakeCount = Math.floor(flakeCountTemp);
        flakeCountTemp -= flakeCount;
        if (Math.random() < flakeCountTemp) {
            flakeCount++;
        }
        var flakeRows = Math.ceil(seconds);
        if (flakeRows == 0) {
            return;
        }
        var flakesPerRow = flakeCount / flakeRows;
        for (var row = 0, i = 0; row < flakeRows && i < flakeCount; row++) {
            for (var j = 0; j < flakesPerRow && i < flakeCount; j++ , i++) {
                var flake = new Flake();
                flake.x = Math.random();
                flake.y = -0.2 + j * this.fallSpeed;
                flake.size = Math.pow(Math.random(), 4) * 3;
                this.flakes.push(flake);
            }
        }
    };
    return Engine;
}());
var Point = /** @class */ (function () {
    function Point() {
    }
    return Point;
}());
var Renderer = /** @class */ (function () {
    function Renderer(engine, document, canvas, label) {
        this.engine = engine;
        this.document = document;
        this.canvas = canvas;
        this.label = label;
        this.sizeFactor = 8;
        this.sizeTerm = 12;
        this.testDiv = document.createElement('div');
        this.localFeatures = {
            transform: {
                ie: this.has('-ms-transform'),
                moz: this.has('MozTransform'),
                opera: this.has('OTransform'),
                webkit: this.has('webkitTransform'),
                w3: this.has('transform'),
                prop: null // the normalized property value
            }
        };
        this.localFeatures.transform.prop = (this.localFeatures.transform.w3 ||
            this.localFeatures.transform.moz ||
            this.localFeatures.transform.webkit ||
            this.localFeatures.transform.ie ||
            this.localFeatures.transform.opera);
        this.isOpacitySupported = this.opacitySupported();
    }
    Renderer.prototype.has = function (prop) {
        // test for feature support
        var result = this.testDiv.style[prop];
        return (result !== undefined ? prop : null);
    };
    Renderer.prototype.start = function () {
        var _this = this;
        this.engine.start();
        this.lastRender = new Date();
        this.intervalHandle = setInterval(function () { return _this.renderFrame(); }, 5);
    };
    Renderer.prototype.stop = function () {
        clearInterval(this.intervalHandle);
    };
    Renderer.prototype.renderFrame = function () {
        this.render();
        if (this.label != null) {
            this.label.textContent = this.engine.flakes.length.toString();
        }
        var now = new Date();
        var seconds = (now.getTime() - this.lastRender.getTime()) / 1000;
        this.lastRender = now;
        this.engine.update(seconds);
        for (var i = 0; i < this.engine.erased.length; i++) {
            var flake = this.engine.erased[i];
            var element = flake.tag;
            element.parentElement.removeChild(element);
        }
    };
    Renderer.prototype.render = function () {
        for (var i = 0; i < this.engine.flakes.length; i++) {
            var flake = this.engine.flakes[i];
            var element = flake.tag;
            if (element == null) {
                element = document.createElement("div");
                element.style.fontSize = (this.sizeTerm + flake.size * this.sizeFactor) + 'px';
                element.style.width = (this.sizeTerm + flake.size * this.sizeFactor) + 'px';
                element.style.height = (this.sizeTerm + flake.size * this.sizeFactor) + 'px';
                //element.style.backgroundColor = '#000';
                element.style.color = '#fff';
                element.style.fontFamily = 'arial,verdana';
                element.style.cursor = 'default';
                element.style.overflow = 'hidden';
                element.style.fontWeight = 'normal';
                element.style.position = 'absolute';
                element.innerHTML = '&bull;';
                element.style[this.localFeatures.transform.prop] = 'translate3d(0px, 0px, 0px)';
                this.canvas.appendChild(element);
                flake.tag = element;
            }
            var point = this.convertFlakeLocation(flake);
            element.style.left = point.x + 'px';
            element.style.top = point.y + 'px';
        }
    };
    Renderer.prototype.convertFlakeLocation = function (flake) {
        //let windFactor: number = this.engine.windDirection * 16000;
        var point = new Point();
        var width = this.canvas.clientWidth - 50;
        var height = this.canvas.clientHeight - 50;
        point.x = flake.x * width; //- windFactor;
        point.y = flake.y * height;
        return point;
    };
    Renderer.prototype.opacitySupported = function () {
        try {
            document.createElement('div').style.opacity = '0.5';
        }
        catch (e) {
            return false;
        }
        return true;
    };
    return Renderer;
}());
var snowStorm = (function (window, document) {
    var engine = new Engine();
    engine.flakesPerSecond = 14;
    engine.fallSpeed = 0.03;
    engine.windDirection = 0.0;
    engine.maxFlakes = 150;
    var canvas = document.getElementById("canvas");
    var textMessageElement = document.getElementById("textMessage");
    var renderer = new Renderer(engine, document, canvas, textMessageElement);
    renderer.start();
}(window, document));
