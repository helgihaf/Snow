﻿using Marson.Flake.Core;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace SnowAscii
{
    internal class ConsoleRenderer
    {
        private ConsoleBuffer consoleBuffer;

        public Engine Engine { get; }

        public int FramesPerSecond { get; set; } = 30;

        public ConsoleRenderer(Engine engine)
        {
            Engine = engine ?? throw new ArgumentNullException(nameof(engine));
        }

        public void Run()
        {
            Console.CursorVisible = false;

            bool doRender = true;
            double frameDurationMilliseconds = 1000d / FramesPerSecond;

            DateTime lastUpdate = DateTime.UtcNow;
            DateTime lastRender = lastUpdate;

            Engine.Start();

            while (doRender)
            {
                var renderStart = DateTime.UtcNow;
                Render();

                var now = DateTime.UtcNow;
                double seconds = (now - lastUpdate).TotalSeconds;
                lastUpdate = now;
                Engine.Update(seconds);

                now = DateTime.UtcNow;
                var renderMilliseconds = (now - lastRender).TotalMilliseconds;
                lastRender = now;

                int remainingMilliseconds = Convert.ToInt32(frameDurationMilliseconds - renderMilliseconds);
                if (remainingMilliseconds > 0)
                {
                    Thread.Sleep(remainingMilliseconds);
                }

                if (Console.KeyAvailable)
                {
                    doRender = false;
                }
            }
        }

        private void Render()
        {
            PrepareConsoleBuffer();
            foreach (var flake in Engine.Flakes)
            {
                switch (flake.State)
                {
                    case FlakeState.New:
                    case FlakeState.Visible:
                        var ascii = flake.Tag as string;
                        if (ascii == null)
                        {
                            if (flake.Size > 0.75)
                            {
                                ascii = "*";
                            }
                            else if (flake.Size > 0.5)
                            {
                                ascii = "+";
                            }
                            else
                            {
                                ascii = ".";
                            }
                            flake.Tag = ascii;
                        }
                        var point = ConvertFlakeLocation(flake);
                        if (0 <= point.X && point.X < consoleBuffer.Width
                            && 0 <= point.Y && point.Y < consoleBuffer.Height)
                        {
                            consoleBuffer.WriteAt(point.X, point.Y, ascii[0]);
                        }

                        if (flake.State == FlakeState.New)
                        {
                            flake.State = FlakeState.Visible;
                        }
                        break;

                    case FlakeState.Hiding:
                        flake.State = FlakeState.Hidden;
                        break;

                    default:
                        break;
                }
            }

            Console.SetCursorPosition(0, 0);
            Console.Write(consoleBuffer.Buffer, 0, consoleBuffer.Buffer.Length - 1);
        }

        private void PrepareConsoleBuffer()
        {
            if (consoleBuffer == null || consoleBuffer.Height != Console.WindowHeight || consoleBuffer.Width != Console.WindowWidth)
            {
                consoleBuffer = new ConsoleBuffer(Console.WindowWidth, Console.WindowHeight);
            }
            else
            {
                consoleBuffer.Clear();
            }
        }

        private Point ConvertFlakeLocation(Marson.Flake.Core.Flake flake)
        {
            return new Point(
                Convert.ToInt32(flake.X * Console.WindowWidth),
                Convert.ToInt32(flake.Y * Console.WindowHeight));
        }

    }
}
