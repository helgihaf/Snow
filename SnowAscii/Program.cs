﻿using Marson.Flake.Core;
using Marsonsoft.CommandLineParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SnowAscii
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = new Parser(new ParserSetup());

            Options options;
            try
            {
                options = parser.Parse<Options>(args);
            }
            catch (Exception ex) when (ex is ArgumentException || ex is FormatException)
            {
                Console.WriteLine(ex.Message);
                ShowHelp(parser);
                return;
            }

            if (options.Help)
            {
                ShowHelp(parser);
                return;
            }

            if (options.Version)
            {
                ShowVersion(parser);
                return;
            }

            var engine = new Engine(new Random(), 500);
            engine.FlakesPerSecond = options.FlakesPerSecond;
            engine.FallSpeed = options.FallSpeed;
            engine.WindDirection = options.WindDirection;

            var renderer = new ConsoleRenderer(engine);
            renderer.FramesPerSecond = options.FramesPerSecond;

            renderer.Run();
        }

        private static void ShowHelp(Parser parser)
        {
            Console.WriteLine();
            Console.WriteLine($"Usage:  dotnet {Assembly.GetExecutingAssembly().GetName().Name} OPTIONS");
            Console.WriteLine();
            Console.WriteLine("Makes your console snow.");
            Console.WriteLine();
            Console.WriteLine("Options:");

            var parameters = parser.GetParameters().ToList();
            var maxParamNameLength = parameters.Max(p => p.Name.Length);

            foreach (var parameter in parameters)
            {
                Console.Write("  ");
                if (parameter.Aliases != null && parameter.Aliases.Length >= 1)
                {
                    Console.Write("-" + parameter.Aliases[0] + ",");
                }
                else
                {
                    Console.Write("   ");
                }
                Console.Write(" ");

                Console.Write("--" + parameter.Name.PadRight(maxParamNameLength, ' ') + " ");
                Console.WriteLine(parameter.Description);
            }
        }

        private static void ShowVersion(Parser parser)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName();
            Console.WriteLine($"{assemblyName.Name} version {assemblyName.Version.ToString(3)}");
        }
    }
}
