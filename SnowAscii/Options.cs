﻿using Marsonsoft.CommandLineParser;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnowAscii
{
    public class Options
    {
        [Parameter(Name = "flakesPerSecond", Description = "Number of flakes created each second. Default 7")]
        public int FlakesPerSecond { get; set; } = 7;

        [Parameter(Name = "fallSpeed", Description = "Median fall speed of a snow flake. Default 0.1")]
        public float FallSpeed { get; set; } = 0.1f;

        [Parameter(Name = "wind", Description = "Wind strength and direction. Negative is left, positive is right. Default 0.01")]
        public float WindDirection { get; set; } = 0.01f;

        [Parameter(Name = "fps", Description = "Target frames per second. Controls update speed and influences CPU utilization. Default 30")]
        public int FramesPerSecond { get; set; } = 30;

        [Parameter(Name = "help", Aliases = new[] { "?" }, Description = "Show help")]
        public bool Help { get; set; }

        [Parameter(Name = "version", Aliases = new[] { "v" }, Description = "Show version")]
        public bool Version { get; set; }
    }
}
