﻿using Marsonsoft.CommandLineParser;
using System.Collections.Generic;
using System.Linq;

namespace SnowAscii
{
    internal static class ParserExtensions
    {
        public static IEnumerable<ParameterAttribute> GetParameters(this Parser parser)
        {
            foreach (var propertyInfo in typeof(Options).GetProperties())
            {
                var parameter = propertyInfo.GetCustomAttributes(typeof(ParameterAttribute), true).FirstOrDefault() as ParameterAttribute;
                if (parameter != null)
                {
                    yield return parameter;
                }
            }
        }
    }
}
