﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnowAscii
{
    internal class ConsoleBuffer
    {
        private char[] buffer;

        public ConsoleBuffer(int width, int height)
        {
            Height = height;
            Width = width;
            CreateBuffer();
        }

        private void CreateBuffer()
        {
            buffer = new char[Height * Width];
        }

        public int Height { get; }
        public int Width { get; }

        public void Clear()
        {
            Array.Fill(buffer, ' ');
        }

        public void WriteAt(int left, int top, char ch)
        {
            int index = top * Width + left;
            buffer[index] = ch;
        }

        public char[] Buffer
        {
            get
            {
                return buffer;
            }
        }
    }
}
