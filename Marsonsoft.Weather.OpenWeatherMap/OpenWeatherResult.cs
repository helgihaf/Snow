﻿namespace Marsonsoft.Weather.OpenWeatherMap
{
    internal class OpenWeatherResult
    {
        public Main Main { get; set; }
        public Wind Wind { get; set; }
    }

    internal class Main
    {
        public double Temp { get; set; }
    }

    internal class Wind
    {
        public double Speed { get; set; }
        public int Deg { get; set; }
    }
}
