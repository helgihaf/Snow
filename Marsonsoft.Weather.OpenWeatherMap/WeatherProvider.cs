﻿using Marsonsoft.Weather.Abstractions;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;


namespace Marsonsoft.Weather.OpenWeatherMap
{
    public class WeatherProvider : IWeatherProvider
    {
        private readonly HttpClient _client = new HttpClient();
        private readonly string _apikey;

        public WeatherProvider(string apikey)
        {
            _apikey = apikey;
        }

        public async Task<Abstractions.Observation> GetLatestObservation(double latitude, double longitude)
        {
            var response = await _client.GetAsync($"https://api.openweathermap.org/data/2.5/weather?lat={latitude.ToString(CultureInfo.InvariantCulture)}&lon={longitude.ToString(CultureInfo.InvariantCulture)}&appid={_apikey}");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var serializationOptions = new System.Text.Json.JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var weather = System.Text.Json.JsonSerializer.Deserialize<OpenWeatherResult>(content, serializationOptions);

            return new Abstractions.Observation
            {
                Temperature = weather.Main.Temp,
                Wind = new Abstractions.Wind
                {
                    Speed = weather.Wind.Speed,
                    Direction = weather.Wind.Deg
                }
            };
        }
    }
}
