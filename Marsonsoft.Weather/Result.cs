﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Weather
{
public class Result
    {
        public string name { get; set; }
        public string time { get; set; }
        public string err { get; set; }
        public string link { get; set; }
        public string F { get; set; }
        public string FX { get; set; }
        public string FG { get; set; }
        public string D { get; set; }
        public string T { get; set; }
        public string W { get; set; }
        public string V { get; set; }
        public string N { get; set; }
        public string P { get; set; }
        public string RH { get; set; }
        public string SNC { get; set; }
        public string SND { get; set; }
        public string SED { get; set; }
        public string RTE { get; set; }
        public string TD { get; set; }
        public string R { get; set; }
        public string id { get; set; }
        public string valid { get; set; }
    }
}
