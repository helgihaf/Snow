﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Weather
{
    public class Observation
    {
        public string Name { get; set; }
        public DateTime? Time { get; set; }
        public int WindMetersPerSecond { get; set; }
        public WindDirection WindDirection { get; set; }
        public double TemperatureCelcius { get; set; }
    }
}
