﻿using System;

namespace Marsonsoft.Weather
{
    public class WindDirection
    {
        private static readonly string[] names = new[]
        {
            "N",
            "NNA",
            "NA",
            "ANA",
            "A",
            "ASA",
            "SA",
            "SSA",
            "S",
            "SSV",
            "SV",
            "VSV",
            "V",
            "VNV",
            "NV",
            "NNV"
        };

        private int index;

        private WindDirection(int index)
        {
            this.index = index;
        }

        public WindDirection(string name)
        {
            index = Array.IndexOf<string>(names, name);
        }

        public string Name => names[index];

        public double Degrees => index * 22.5;

        public WindDirection Next => new WindDirection((index + 1) % names.Length);

        public WindDirection Prev
        {
            get
            {
                int newIndex = index - 1;
                if (newIndex < 0)
                {
                    newIndex = names.Length - 1;
                }

                return new WindDirection(newIndex);
            }
        }
    }
}
