﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;

namespace Marsonsoft.Weather
{
    public class WeatherProvider
    {

        private readonly HttpClient client = new HttpClient();

        public class ResultContainer
        {
            public IEnumerable<Result> Results { get; set; }
        }
        public WeatherProvider(Uri uri)
        {
            client.BaseAddress = uri ?? throw new ArgumentNullException(nameof(uri));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Observation> GetLatestObservationAsync(int stationId)
        {
            Observation observation = null;
            HttpResponseMessage response = await client.GetAsync(client.BaseAddress + $"/observations/is?stations={stationId}");
            if (response.IsSuccessStatusCode)
            {
                string observationString = await response.Content.ReadAsStringAsync();
                var resultContainer = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultContainer>(observationString);
                observation = ToObservation(resultContainer.Results.FirstOrDefault());
            }
            return observation;
        }

        private Observation ToObservation(Result result)
        {
            if (result == null)
            {
                return null;
            }

            return new Observation
            {
                Name = result.name,
                Time = result.time != null ? DateTime.ParseExact(result.time, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) : (DateTime?)null,
                WindMetersPerSecond = int.Parse(result.F),
                WindDirection = new WindDirection(result.D),
                TemperatureCelcius = double.Parse(result.T, CultureInfo.InvariantCulture)
            };
        }
    }
}
