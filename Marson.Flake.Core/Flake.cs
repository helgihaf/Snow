﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marson.Flake.Core
{
    public class Flake
    {
        /// <summary>
        /// Size on the scale of 0 to 3
        /// </summary>
        public double Size;

        /// <summary>
        /// X-coordinate (left) on the scale of 0 to 1
        /// </summary>
        public double X;

        /// <summary>
        /// Y-coordinate (top) on the scale of -0.2 to 1
        /// </summary>
        public double Y;

        public FlakeState State { get; set; }

        public object Tag;
    }
}
