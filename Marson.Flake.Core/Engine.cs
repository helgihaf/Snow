﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Marson.Flake.Core
{
    public class Engine
    {
        private readonly Random random;
        private Flake[] flakes;

        public Engine(Random random, int maxFlakes)
        {
            if (random == null)
            {
                throw new ArgumentNullException(nameof(random));
            }
            this.random = random;
            MaxFlakes = maxFlakes;
        }

        public IReadOnlyCollection<Flake> Flakes
        {
            get { return flakes; }
        }

        public int MaxFlakes { get; }
        public double FlakesPerSecond { get; set; }
        public double FallSpeed { get; set; }
        public double WindDirection { get; set; }

        public void Start()
        {
            flakes = new Flake[MaxFlakes];
            for (int i = 0; i < flakes.Length; i++)
            {
                flakes[i] = new Flake
                {
                    State = FlakeState.Hidden
                };
            }
        }

        public void Update(double elapsedSeconds)
        {
            MoveExistingFlakes(elapsedSeconds);
            AddNewFlakes(elapsedSeconds);
        }

        private void MoveExistingFlakes(double seconds)
        {
            Parallel.ForEach(flakes, flake =>
            {
                if (flake.State == FlakeState.Visible)
                {
                    ApplyWind(seconds, flake);
                    ApplyGravity(seconds, flake);

                    if (flake.Y > 1)
                    {
                        flake.State = FlakeState.Hiding;
                    }
                }
            });
        }

        private void ApplyWind(double seconds, Flake flake)
        {
            flake.X += WindDirection * seconds;
        }

        private void ApplyGravity(double seconds, Flake flake)
        {
            flake.Y += 0.001 + FallSpeed * seconds * flake.Size;
        }

        public void AddNewFlakes(double seconds)
        {
            double flakeCountTemp = FlakesPerSecond * seconds;
            int flakeCount = Convert.ToInt32(Math.Truncate(flakeCountTemp));
            flakeCountTemp -= flakeCount;
            if (random.NextDouble() < flakeCountTemp)
            {
                flakeCount++;
            }
            int flakeRows = Convert.ToInt32(Math.Ceiling(seconds));
            if (flakeRows == 0)
            {
                return;
            }
            int flakesPerRow = flakeCount / flakeRows;
            int flakeIndex = -1;
            for (int row = 0, i = 0; row < flakeRows && i < flakeCount; row++)
            {
                for (int j = 0; j < flakesPerRow && i < flakeCount; j++, i++)
                {
                    flakeIndex = FindNextHiddenFlake(flakeIndex);
                    if (flakeIndex >= 0)
                    {
                        var flake = flakes[flakeIndex];
                        flake.X = random.NextDouble() * 2.0 - 0.5;
                        flake.Y = -0.2 + j * FallSpeed;
                        flake.Size = Math.Pow(random.NextDouble(), 4) * 3;
                        flake.State = FlakeState.New;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }

        private int FindNextHiddenFlake(int flakeIndex)
        {
            do
            {
                flakeIndex++;
            } while (flakeIndex < flakes.Length && flakes[flakeIndex].State != FlakeState.Hidden);

            if (flakeIndex < flakes.Length)
            {
                return flakeIndex;
            }

            return -1;
        }
    }
}
