﻿namespace Marson.Flake.Core
{
    public enum FlakeState
    {
        New,
        Visible,
        Hiding,
        Hidden
    }
}
