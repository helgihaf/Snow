﻿using Marson.Flake.Core;
using Marson.Flake.Wpf;
using Marsonsoft.Weather.Abstractions;
using Marsonsoft.Weather.OpenWeatherMap;
using System;
using System.Configuration;
using System.Device.Location;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Snow2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const double DefaultWindDirection = 0.01;
        private readonly Engine engine;
        private readonly Renderer renderer;
        private readonly WeatherProvider weatherProvider;
        private readonly System.Windows.Threading.DispatcherTimer weatherUpdateTimer = new System.Windows.Threading.DispatcherTimer();
        private bool realWind = true;
        private Observation weatherObservation;
        private GeoCoordinateWatcher geoCoordinateWatcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
        private GeoCoordinate lastKnownLocation = GeoCoordinate.Unknown;
        private DateTime lastGeoLocationStatusChange = DateTime.MinValue;

        public MainWindow()
        {
            InitializeComponent();

            menuItemRealWind.IsChecked = realWind;

            string apiKey = ReadApiKeyFromAppConfig();
            weatherProvider = new WeatherProvider(apiKey);
            engine = new Engine(new Random(), 500);
            engine.FlakesPerSecond = 30;
            engine.FallSpeed = 0.03;
            engine.WindDirection = DefaultWindDirection;
            renderer = new Renderer(canvas, engine, null);
            renderer.Start();

            geoCoordinateWatcher.StatusChanged += GeCoordinateWatcher_StatusChanged;
            geoCoordinateWatcher.Start();

            weatherUpdateTimer.Tick += DispatcherTimer_Tick;
            weatherUpdateTimer.Interval = new TimeSpan(0, 20, 0);
            weatherUpdateTimer.Start();
        }

        private string ReadApiKeyFromAppConfig()
        {
            return ConfigurationManager.AppSettings["WeatherApiKey"];
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            UpdateWeather();
        }

        private void UpdateWeather()
        {
            if (realWind)
            {
                Task.Factory.StartNew(async () => await UpdateWeatherAsync()).Wait();
            }
            else
            {
                engine.WindDirection = DefaultWindDirection;
            }
        }

        private async Task UpdateWeatherAsync()
        {
            if (lastKnownLocation == GeoCoordinate.Unknown)
            {
                return;
            }

            try
            {
                weatherObservation = await weatherProvider.GetLatestObservation(lastKnownLocation.Latitude, lastKnownLocation.Longitude);
                UpdateEngineWithWeather(weatherObservation);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void UpdateEngineWithWeather(Observation observation)
        {
            if (observation == null)
            {
                return;
            }

            //Debug.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(observation));
            double projectedDegrees = (observation.Wind.Direction + 180 + 45) % 360;
            double leftRightFactor = Math.Sin(ConvertToRadians(projectedDegrees));
            double strengthFactor = Convert.ToDouble(observation.Wind.Speed) / 80d;
            engine.WindDirection = -leftRightFactor * strengthFactor;

            Dispatcher.Invoke(new Action(() => {
                labelWind.Content = $"{observation.Wind.Direction}° {observation.Wind.Speed} m/s";
                labelTemperature.Content = $"{KelvinToCelcius(observation.Temperature):F1} °C";
                // Rotate the wind arrow
                RotateTransform rotateTransform = new RotateTransform(observation.Wind.Direction);
                windArrow.RenderTransform = rotateTransform;
            }));
        }

        private double KelvinToCelcius(double temperature)
        {
            return temperature - 273.15;
        }

        public double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }

        private void canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                if (WindowState == WindowState.Maximized)
                {
                    WindowState = WindowState.Normal;
                    WindowStyle = WindowStyle.SingleBorderWindow;
                    ResizeMode = ResizeMode.CanResize;
                }
                else if (WindowState == WindowState.Normal)
                {
                    WindowState = WindowState.Maximized;
                    WindowStyle = WindowStyle.None;
                    ResizeMode = ResizeMode.NoResize;
                }
            }
        }

        private void MenuItemRealWind_Click(object sender, RoutedEventArgs e)
        {
            realWind = menuItemRealWind.IsChecked;
            UpdateWeather();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (weatherObservation == null)
            {
                return;
            }

            if (e.Key == Key.Up)
            {
                weatherObservation.Wind.Speed++;
                UpdateEngineWithWeather(weatherObservation);
            }
            else if (e.Key == Key.Down)
            {
                weatherObservation.Wind.Speed--;
                UpdateEngineWithWeather(weatherObservation);
            }
            else if (e.Key == Key.Left)
            {
                weatherObservation.Wind.Direction--;
                UpdateEngineWithWeather(weatherObservation);
            }
            else if (e.Key == Key.Right)
            {
                weatherObservation.Wind.Direction++;
                UpdateEngineWithWeather(weatherObservation);
            }
        }

        private void GeCoordinateWatcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            if (e.Status == GeoPositionStatus.Ready && lastGeoLocationStatusChange.AddMinutes(20) < DateTime.UtcNow)
            {
                lastKnownLocation = geoCoordinateWatcher.Position.Location;
                lastGeoLocationStatusChange = DateTime.UtcNow;
                UpdateWeather();
            }
        }
    }
}
