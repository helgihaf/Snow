﻿using Marson.Flake.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Marson.Flake.Wpf
{
    public class Renderer
    {
        public const int LeftRightFrame = 800;

        private readonly Canvas canvas;
        private readonly Engine engine;
        private readonly Label label;

        private DateTime lastRender;

        public double SizeFactor { get; set; } = 2;

        public double SizeTerm { get; set; } = 2;


        public Renderer(Canvas canvas, Engine engine, Label label)
        {
            if (canvas == null)
            {
                throw new ArgumentNullException(nameof(canvas));
            }
            this.canvas = canvas;

            if (engine == null)
            {
                throw new ArgumentNullException(nameof(engine));
            }
            this.engine = engine;
            this.label = label;
        }

        public void Start()
        {
            CompositionTarget.Rendering += CompositionTarget_Rendering;
            engine.Start();
            foreach (var flake in engine.Flakes)
            {
                var snowflake = new Ellipse();
                flake.Tag = snowflake;
                canvas.Children.Add(snowflake);
            }
            lastRender = DateTime.UtcNow;
        }

        public void Stop()
        {
            CompositionTarget.Rendering -= CompositionTarget_Rendering;
        }

        private void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            Render();
            if (label != null)
            {
                label.Content = engine.Flakes.Count();
            }

            var now = DateTime.UtcNow;
            double seconds = (now - lastRender).TotalSeconds;
            lastRender = now;
            engine.Update(seconds);
        }

        private void Render()
        {
            foreach (var flake in engine.Flakes)
            {
                switch (flake.State)
                {
                    case FlakeState.New:
                        InitializeSnowflake(flake);
                        UpdateSnowflake(flake);
                        break;
                    case FlakeState.Visible:
                        UpdateSnowflake(flake);
                        break;
                    case FlakeState.Hiding:
                        HideSnowflake(flake);
                        break;
                }
            }
        }

        private void InitializeSnowflake(Core.Flake flake)
        {
            var ellipse = flake.Tag as Ellipse;
            ellipse.Width = SizeTerm + flake.Size * SizeFactor;
            ellipse.Height = SizeTerm + flake.Size * SizeFactor;
            ellipse.Fill = Brushes.White;
            flake.State = FlakeState.Visible;
        }

        private void UpdateSnowflake(Core.Flake flake)
        {
            var ellipse = flake.Tag as Ellipse;
            var point = ConvertFlakeLocation(flake);
            Canvas.SetLeft(ellipse, point.X);
            Canvas.SetTop(ellipse, point.Y);
        }

        private void HideSnowflake(Core.Flake flake)
        {
            flake.X = 2.0;
            flake.Y = 2.0;
            UpdateSnowflake(flake);
            flake.State = FlakeState.Hidden;
        }

        private Point ConvertFlakeLocation(Marson.Flake.Core.Flake flake)
        {
            double windFactor = engine.WindDirection * 16000;
            return new Point(
                flake.X * (canvas.RenderSize.Width) - windFactor,
                flake.Y * canvas.RenderSize.Height);
        }

    }
}