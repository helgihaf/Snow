﻿namespace Marsonsoft.Weather.Abstractions
{
    public class Observation
    {
        public Wind Wind { get; set; }

        /// <summary>
        /// Kelvin
        /// </summary>
        public double Temperature { get; set; }
    }
}
