﻿using System.Threading.Tasks;

namespace Marsonsoft.Weather.Abstractions
{
    public interface IWeatherProvider
    {
        Task<Observation> GetLatestObservation(double latitude, double longitude);
    }
}
