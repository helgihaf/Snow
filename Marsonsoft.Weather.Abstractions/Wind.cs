﻿using System;

namespace Marsonsoft.Weather.Abstractions
{
    public class Wind
    {
        /// <summary>
        /// Meters/second
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// Degrees
        /// </summary>
        public int Direction { get; set; }
    }
}
