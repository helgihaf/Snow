﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Marsonsoft.Weather.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var provider = new WeatherProvider(new Uri("http://apis.is/weather"));
            var observation = provider.GetLatestObservationAsync(1477).Result;
            Assert.IsNotNull(observation);
        }
    }
}
